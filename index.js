//require module
const express = require('express');


//allow us to connect in our mongodb
const mongoose = require('mongoose');

//port
const port = 4000;
//server
const app = express();

//mongoose connection/ database connection
 // use connect method
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.arbsv.mongodb.net/tasks182?retryWrites=true&w=majority",{

		//prevent errors in future upd
		useNewUrlParser: true,
		useUnifiedTopology: true
}

);

//this will create a notification if the db connection is successful or not
let db = mongoose.connection


//handling of events , data event
//console.error.bind - show error in terminal and browser
db.on('error', console.error.bind(console,"DB Connection Error"));


//when event open, output message
db.once('open', () => console.log("Successfully connected to MongoDB"));




//middlewares
app.use(express.json());

//mw used to form; take place to value we get from forms -- urlencoded, absorb/accept any data-type; reading of data forms
app.use(express.urlencoded({extended: true}));


//Routes (Grouping routes)
const taskRoutes = require('./routes/taskRoutes');
app.use('/tasks', taskRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);


//port listener
app.listen(port, () => console.log(`Server is running at port ${port}.`));

