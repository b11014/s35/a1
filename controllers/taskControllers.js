//logic that we want to imply

// import task model - controllers function may have access to our task model

const Task = require("../models/Task");

//create task, if you didnt create task you cant perform other operations
//so you can call the controller to routes

module.exports.createTaskController = (req, res) => {
	console.log(req.body);

	//no duplicate task
	//call model, what name you input find it
	//mongoDB: db.tasks.findOne({name: "nameOfTask"})

	//.then a ex. of promise; way to make asynchronous the function
	// promise - result to fulfilled promise, but it can have an error or not fulfilled
	Task.findOne({name: req.body.name}).then(result => {

		console.log(result);
		if(result !== null && result.name === req.body.name){
			return res.send('Duplicate task found') 
		} else {

			//save method
			let newTask = new Task({
				name: req.body.name,
				status: req.body.status
			})

			newTask.save().then(result => res.send(result)).catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};

//retrieve all tasks

module.exports.getAllTasksController = (req, res) => {


//similar: db.tasks.find({})

	Task.find({}).then(result => res.send(result)).catch(error => res.send(error));
};

//retrieval single task

module.exports.getSingleTaskController = (req, res) => {
	console.log(req.params);

	
	// db.tasks.findOne({_id: "id"}) <- mongodb Method
	//findById mongoose method

	Task.findById(req.params.id).then(result => res.send(result)).catch(error => res.send(error));
};


//params url
//parameters function

//updating task status

module.exports.updateTasksStatusController = (req, res) => {
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		status: req.body.status
	};

	//findbyId and Update()
		//3 arguments
		//a. where will you get the id? req.params.id
		//b. what is the update? updates variable
		//c. optional: {new: true}- return the updated version of the document we are updating 
	Task.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedTask => res.send(updatedTask))
	.catch(err => res.send(err));
};
