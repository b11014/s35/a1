const express = require('express');
const router = express.Router();
// router method allows access to HTTP methods

const taskControllers = require('../controllers/taskControllers');

console.log(taskControllers);

//create task route
router.post('/', taskControllers.createTaskController);

// get all tasks route
router.get('/', taskControllers.getAllTasksController);


// get single task using wildcard/index
router.get('/getSingleTask/:id', taskControllers.getSingleTaskController);

//update task status
router.put('/updateTask/:id', taskControllers.updateTasksStatusController);



//connected to the server (index.js)
module.exports = router;
