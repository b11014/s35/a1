const express = require('express');
const router = express.Router();

const userControllers = require('../controllers/userControllers');

console.log(userControllers);


router.post('/', userControllers.createUserController)

router.get('/', userControllers.getAllUsersController)


//act

router.get('/getSingleUser/:id', userControllers.getSingleUserController)

router.put('/updateUser/:id', userControllers.updateUsernameController)


module.exports = router;
