//express - what you need is what you require

const mongoose = require('mongoose');

// declaring schema
// Schema - blueprint for our data or document , constructor, structure


const taskSchema = new mongoose.Schema({

	name: String,

	status: String
});

//Mongoose Model
/*

	mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)
	
*/

//so we can use the module outside the js
//model is use for manipulation; manipulating obj
//task = collection; holds docs
// naming convention, shouldnt be plural
module.exports = mongoose.model("Task", taskSchema);

